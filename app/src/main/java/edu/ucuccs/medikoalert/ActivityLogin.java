package edu.ucuccs.medikoalert;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import edu.ucuccs.medikoalert.custom.CustomRequest;

/**
 * A login screen that offers login via email/password.
 */
public class ActivityLogin extends AppCompatActivity {

    @BindView(R.id.edtEmail) TextInputEditText edtEmail;
    @BindView(R.id.edtPassword) TextInputEditText edtPassword;
    @BindView(R.id.btnLogin) Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkLoginCredential();
            }
        });
    }

    //Method to check if email and password exists
    private void checkLoginCredential(){
        final ProgressDialog ploading = ProgressDialog.show(this, null, "Logging in...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Credentials.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean error = jsonResponse.getBoolean("error");
                    String message = jsonResponse.getString("message");
                    if(!error){
                        ploading.dismiss();
                        //Display valid message here

                        Toast.makeText(ActivityLogin.this, message, Toast.LENGTH_SHORT).show();

                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();

                    }else{
                        ploading.dismiss();
                        Toast.makeText(ActivityLogin.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError volleyError){
                        ploading.dismiss();
                        Toast.makeText(getApplicationContext(), "Cannot connect to the server", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams() throws AuthFailureError {
                String email = edtEmail.getText().toString();
                String password = edtPassword.getText().toString();

                Map<String,String>params = new Hashtable <String,String>();
                params.put("username", email);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue  = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20*1000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}

